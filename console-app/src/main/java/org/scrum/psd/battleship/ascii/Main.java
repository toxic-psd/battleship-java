package org.scrum.psd.battleship.ascii;

import com.diogonunes.jcdp.color.ColoredPrinter;
import com.diogonunes.jcdp.color.api.Ansi;
import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {
    private static List<Ship> myFleet;
    private static List<Ship> enemyFleet;
    private static ColoredPrinter console;

    public static void main(String[] args) {
        console = new ColoredPrinter.Builder(1, false).background(Ansi.BColor.BLACK).foreground(Ansi.FColor.WHITE).build();

        colorAction(Ansi.FColor.MAGENTA, () -> {
            console.println("                                     |__");
            console.println("                                     |\\/");
            console.println("                                     ---");
            console.println("                                     / | [");
            console.println("                              !      | |||");
            console.println("                            _/|     _/|-++'");
            console.println("                        +  +--|    |--|--|_ |-");
            console.println("                     { /|__|  |/\\__|  |--- |||__/");
            console.println("                    +---------------___[}-_===_.'____                 /\\");
            console.println("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _");
            console.println(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7");
            console.println("|                        Welcome to Battleship                         BB-61/");
            console.println(" \\_________________________________________________________________________|");
            console.println("");
        });

        InitializeGame();

        StartGame();
    }

    private static void StartGame() {
        Scanner scanner = new Scanner(System.in);

        console.println("          ________  ");
        console.println("      (( /========\\ ))");
        console.println("      __/__________\\____________n_");
        console.println("    (( /             \\_____________]");
        console.println("     /  =(*)=         \\");
        console.println("    |_._._._._._._._._|\\");
        console.println("(( / __________________ \\ ))");
        console.println("  | OOOOOOOOOOOOOOOOOOO0 |  ");

        do {
            console.println("---------------------------------");

            console.println("");
            console.println("Player, it's your turn");
            colorAction(Ansi.FColor.YELLOW, () -> console.println("Enter coordinates for your shot :"));
            Position position = tryToHit(scanner);
            boolean isHit = GameController.checkIsHit(enemyFleet, position);
            if (isHit) {
                hit();
                Ship destroyedShip = GameController.injureShips(enemyFleet, position);
                if (destroyedShip != null) {
                    console.println("---------------------------------");
                    colorAction(Ansi.FColor.GREEN, () -> console.println(destroyedShip.getName() + " DESTROYED"));
                    console.println("---------------------------------");
                }
            } else {
                water();
            }

            console.println(isHit ? "Yeah ! Nice hit !" : "Miss");

            boolean gameOver = true;
            for (Ship ship : enemyFleet) {
                if (ship.isLived()) {
                    gameOver = false;
                    console.println("Enemy " + ship.getName() + " IS ALIVE");
                }
            }
            if (gameOver) {
                colorAction(Ansi.FColor.GREEN, () -> console.println("YOU WIN"));
                return;
            }

            console.println("---------------------------------");
            position = getRandomPosition();
            isHit = GameController.checkIsHit(myFleet, position);
            console.println("");
            console.println(String.format("Computer shoot in %s%s and %s", position.getColumn(), position.getRow(), isHit ? "hit your ship !" : "miss"));
            if (isHit) {
                hit();
                Ship destroyedShip = GameController.injureShips(myFleet, position);
                if (destroyedShip != null) {
                    console.println("---------------------------------");
                    colorAction(Ansi.FColor.RED, () -> console.println(destroyedShip.getName() + " DESTROYED"));
                    console.println("---------------------------------");
                }
            } else {
                water();
            }

            gameOver = true;
            for (Ship ship : myFleet) {
                if (ship.isLived()) {
                    gameOver = false;
                    console.println("My " + ship.getName() + " IS ALIVE");
                }
            }
            if (gameOver) {
                colorAction(Ansi.FColor.RED, () -> console.println("YOU LOST"));
                return;
            }
        } while (true);
    }

    private static Position tryToHit(Scanner scanner) {
        Position position = parsePosition(scanner.next());

        if (position == null) {
            colorAction(Ansi.FColor.RED, () -> console.println("Wrong value. Enter coordinates for your shot :"));
            return tryToHit(scanner);
        }
        return position;
    }

    private static void hit() {
        beep();
        colorAction(Ansi.FColor.YELLOW, Main::boom);
    }

    private static void water() {
        beep();
        colorAction(Ansi.FColor.CYAN, Main::miss);
    }

    private static void boom() {
        console.println("          _ ._  _ , _ ._");
        console.println("        (_ ' ( `  )_  .__)");
        console.println("      ( (  (    )   `)  ) _)");
        console.println("     (__ (_   (_ . _) _) ,__)");
        console.println("         `~~`\\ ' . /`~~`");
        console.println("              ;   ;");
        console.println("              /   \\");
        console.println("_____________/_ __ \\_____________");
    }

    private static void miss() {
        console.println("                \\         .  ./");
        console.println("              \\      .:\" \";'.:..\" \"   /");
        console.println("                  (M^^.^~~:.'\" \").");
        console.println("            -   (/  .    . . \\ \\)  -");
        console.println("               ((| :. ~ ^  :. .|))");
        console.println("            -   (\\- |  \\ /  |  /)  -");
        console.println("                 -\\  \\     /  /-");
        console.println("                   \\  \\   /  /");
    }

    private static void beep() {
        console.setForegroundColor(Ansi.FColor.WHITE);
        console.print("\007");
    }

    protected static Position parsePosition(String input) {
        try {
            Letter letter = Letter.valueOf(input.toUpperCase().substring(0, 1));
            int number = Integer.parseInt(input.substring(1));
            if (number < 1 || number > 8) {
                return null;
            }
            return new Position(letter, number);
        } catch (Exception e) {
            return null;
        }
    }

    private static Position getRandomPosition() {
        int rows = 7;
        int lines = 7;
        Random random = new Random();
        Letter letter = Letter.values()[random.nextInt(lines)];
        int number = random.nextInt(rows) + 1;
        return new Position(letter, number);
    }


    private static void InitializeGame() {
        console.setBackgroundColor(Ansi.BColor.BLACK);
        InitializeMyFleet();
        InitializeEnemyFleet();
    }

    private static void InitializeMyFleet() {
        Scanner scanner = new Scanner(System.in);
        myFleet = GameController.initializeShips();

        console.println("Please position your fleet (Game board has size from A to H and 1 to 8) :");

        for (Ship ship : myFleet) {
            addShip(scanner, ship);
        }
    }

    private static void addShip(Scanner scanner, Ship ship) {
        console.println("");
        console.println(String.format("Please enter the start positions and direction for the %s (size: %s)", ship.getName(), ship.getSize()));

        colorAction(Ansi.FColor.YELLOW, () -> console.println("Enter the top left position (i.e A3):"));
        Position position = tryToSetPosition(scanner);

        colorAction(Ansi.FColor.YELLOW, () -> console.println("Enter the direction (i.e R,D):"));
        Direction direction = tryToSetDirection(scanner);

        ArrayList<Position> positions;
        try {
            positions = getPositions(position, ship.getSize(), direction);
        } catch (IllegalArgumentException e) {
            colorAction(Ansi.FColor.RED, () -> console.println("Position not on the battlefield. TRY AGAIN"));
            addShip(scanner, ship);
            return;
        }

        // тут добавляем кораблю позиции
        boolean isOk = validatePositions(positions);
        if (!isOk) {
            colorAction(Ansi.FColor.RED, () -> console.println("Position is already taken. Plz try again"));
            addShip(scanner, ship);
            return;
        }

        ship.setPositions(positions);
        colorAction(Ansi.FColor.GREEN, () -> console.println("Ship has been placed"));
        colorAction(Ansi.FColor.GREEN, () -> console.println("Your ships:"));

        for (Ship s : myFleet) {
            if (s.getPositions().isEmpty()) {
                continue;
            }
            String collect = s.getPositions().stream().map(p -> p.getColumn().name() + p.getRow()).collect(Collectors.joining(","));
            colorAction(Ansi.FColor.GREEN, () -> console.println(s.getName() + ": " + collect));
        }
    }


    private static ArrayList<Position> getPositions(Position position, int size, Direction direction) {
        ArrayList<Position> positions = new ArrayList<>();

        if (direction.equals(Direction.D)) {
            for (int i = position.getRow(); i < position.getRow() + size; i++) {
                if (i > 8) {
                    throw new IllegalArgumentException("Invalid row number");
                }
                positions.add(new Position(position.getColumn(), i));
            }
        } else {
            for (int i = position.getColumn().ordinal(); i < position.getColumn().ordinal() + size; i++) {
                if (i > 7) {
                    throw new IllegalArgumentException("Invalid column");
                }
                positions.add(new Position(Letter.values()[i], position.getRow()));
            }
        }

        return positions;
    }

    private static boolean validatePositions(ArrayList<Position> positions) {
        for (Position position : positions) {
            if (!validatePosition(position)) {
                return false;
            }
        }
        return true;
    }

    private static boolean validatePosition(Position position) {
        for (Ship ship : myFleet) {
            List<Position> positions = ship.getPositions();
            for (Position p : positions) {
                if (p.equals(position))
                    return false;
            }
        }

        return true;
    }

    private static Position tryToSetPosition(Scanner scanner) {
        Position position = parsePosition(scanner.next());

        if (position == null) {
            colorAction(Ansi.FColor.RED, () -> console.println("Wrong value. Please enter correct position (from A to H and 1 to 8):"));
            return tryToHit(scanner);
        }
        return position;
    }

    private static Direction tryToSetDirection(Scanner scanner) {
        String inputString = scanner.next();

        if (inputString.toUpperCase().equals("R") || inputString.toUpperCase().equals("D")) {
            return Direction.valueOf(inputString.toUpperCase());
        } else {
            colorAction(Ansi.FColor.RED, () -> console.println("Wrong value. Please enter correct position (R or D):"));
            return tryToSetDirection(scanner);
        }
    }

    private static void InitializeEnemyFleet() {
        enemyFleet = GameController.initializeShips();
        initializeShipsPositionRandomly();
    }

    private static void initializeShipsPositionRandomly() {
        Random random = new Random();
        int number = random.nextInt(5);
        switch (number) {
            case (0): {
                enemyFleet.get(0).getPositions().add(new Position(Letter.B, 1));
                enemyFleet.get(0).getPositions().add(new Position(Letter.C, 1));
                enemyFleet.get(0).getPositions().add(new Position(Letter.D, 1));
                enemyFleet.get(0).getPositions().add(new Position(Letter.E, 1));
                enemyFleet.get(0).getPositions().add(new Position(Letter.F, 1));

                enemyFleet.get(1).getPositions().add(new Position(Letter.B, 3));
                enemyFleet.get(1).getPositions().add(new Position(Letter.B, 4));
                enemyFleet.get(1).getPositions().add(new Position(Letter.B, 5));
                enemyFleet.get(1).getPositions().add(new Position(Letter.B, 6));

                enemyFleet.get(2).getPositions().add(new Position(Letter.D, 7));
                enemyFleet.get(2).getPositions().add(new Position(Letter.E, 7));
                enemyFleet.get(2).getPositions().add(new Position(Letter.F, 7));

                enemyFleet.get(3).getPositions().add(new Position(Letter.D, 5));
                enemyFleet.get(3).getPositions().add(new Position(Letter.E, 5));
                enemyFleet.get(3).getPositions().add(new Position(Letter.F, 5));

                enemyFleet.get(4).getPositions().add(new Position(Letter.H, 2));
                enemyFleet.get(4).getPositions().add(new Position(Letter.H, 3));
                break;
            }
            case (1): {
                enemyFleet.get(0).getPositions().add(new Position(Letter.B, 8));
                enemyFleet.get(0).getPositions().add(new Position(Letter.C, 8));
                enemyFleet.get(0).getPositions().add(new Position(Letter.D, 8));
                enemyFleet.get(0).getPositions().add(new Position(Letter.E, 8));
                enemyFleet.get(0).getPositions().add(new Position(Letter.F, 8));

                enemyFleet.get(1).getPositions().add(new Position(Letter.D, 2));
                enemyFleet.get(1).getPositions().add(new Position(Letter.E, 2));
                enemyFleet.get(1).getPositions().add(new Position(Letter.F, 2));
                enemyFleet.get(1).getPositions().add(new Position(Letter.G, 2));

                enemyFleet.get(2).getPositions().add(new Position(Letter.E, 4));
                enemyFleet.get(2).getPositions().add(new Position(Letter.F, 4));
                enemyFleet.get(2).getPositions().add(new Position(Letter.G, 4));

                enemyFleet.get(3).getPositions().add(new Position(Letter.A, 5));
                enemyFleet.get(3).getPositions().add(new Position(Letter.B, 5));
                enemyFleet.get(3).getPositions().add(new Position(Letter.C, 5));

                enemyFleet.get(4).getPositions().add(new Position(Letter.H, 6));
                enemyFleet.get(4).getPositions().add(new Position(Letter.H, 7));
                break;
            }
            case (2): {
                enemyFleet.get(0).getPositions().add(new Position(Letter.B, 6));
                enemyFleet.get(0).getPositions().add(new Position(Letter.C, 6));
                enemyFleet.get(0).getPositions().add(new Position(Letter.D, 6));
                enemyFleet.get(0).getPositions().add(new Position(Letter.E, 6));
                enemyFleet.get(0).getPositions().add(new Position(Letter.F, 6));

                enemyFleet.get(1).getPositions().add(new Position(Letter.G, 1));
                enemyFleet.get(1).getPositions().add(new Position(Letter.G, 2));
                enemyFleet.get(1).getPositions().add(new Position(Letter.G, 3));
                enemyFleet.get(1).getPositions().add(new Position(Letter.G, 4));

                enemyFleet.get(2).getPositions().add(new Position(Letter.C, 2));
                enemyFleet.get(2).getPositions().add(new Position(Letter.D, 2));
                enemyFleet.get(2).getPositions().add(new Position(Letter.E, 2));

                enemyFleet.get(3).getPositions().add(new Position(Letter.A, 2));
                enemyFleet.get(3).getPositions().add(new Position(Letter.A, 3));
                enemyFleet.get(3).getPositions().add(new Position(Letter.A, 4));

                enemyFleet.get(4).getPositions().add(new Position(Letter.H, 7));
                enemyFleet.get(4).getPositions().add(new Position(Letter.H, 8));
                break;
            }
            case (3): {
                enemyFleet.get(0).getPositions().add(new Position(Letter.B, 2));
                enemyFleet.get(0).getPositions().add(new Position(Letter.C, 2));
                enemyFleet.get(0).getPositions().add(new Position(Letter.D, 2));
                enemyFleet.get(0).getPositions().add(new Position(Letter.E, 2));
                enemyFleet.get(0).getPositions().add(new Position(Letter.F, 2));

                enemyFleet.get(1).getPositions().add(new Position(Letter.F, 5));
                enemyFleet.get(1).getPositions().add(new Position(Letter.F, 6));
                enemyFleet.get(1).getPositions().add(new Position(Letter.F, 7));
                enemyFleet.get(1).getPositions().add(new Position(Letter.F, 8));

                enemyFleet.get(2).getPositions().add(new Position(Letter.B, 7));
                enemyFleet.get(2).getPositions().add(new Position(Letter.C, 7));
                enemyFleet.get(2).getPositions().add(new Position(Letter.D, 7));

                enemyFleet.get(3).getPositions().add(new Position(Letter.H, 3));
                enemyFleet.get(3).getPositions().add(new Position(Letter.H, 4));
                enemyFleet.get(3).getPositions().add(new Position(Letter.H, 5));

                enemyFleet.get(4).getPositions().add(new Position(Letter.A, 4));
                enemyFleet.get(4).getPositions().add(new Position(Letter.A, 5));
                break;
            }
            case (4): {
                enemyFleet.get(0).getPositions().add(new Position(Letter.H, 2));
                enemyFleet.get(0).getPositions().add(new Position(Letter.H, 3));
                enemyFleet.get(0).getPositions().add(new Position(Letter.H, 4));
                enemyFleet.get(0).getPositions().add(new Position(Letter.H, 5));
                enemyFleet.get(0).getPositions().add(new Position(Letter.H, 6));

                enemyFleet.get(1).getPositions().add(new Position(Letter.E, 5));
                enemyFleet.get(1).getPositions().add(new Position(Letter.E, 6));
                enemyFleet.get(1).getPositions().add(new Position(Letter.E, 7));
                enemyFleet.get(1).getPositions().add(new Position(Letter.E, 8));

                enemyFleet.get(2).getPositions().add(new Position(Letter.B, 1));
                enemyFleet.get(2).getPositions().add(new Position(Letter.C, 1));
                enemyFleet.get(2).getPositions().add(new Position(Letter.D, 1));

                enemyFleet.get(3).getPositions().add(new Position(Letter.C, 3));
                enemyFleet.get(3).getPositions().add(new Position(Letter.D, 3));
                enemyFleet.get(3).getPositions().add(new Position(Letter.E, 3));

                enemyFleet.get(4).getPositions().add(new Position(Letter.A, 6));
                enemyFleet.get(4).getPositions().add(new Position(Letter.A, 7));
                break;
            }
        }
    }


    @FunctionalInterface
    public interface ColoredAction {
        void execute();
    }

    public static void colorAction(Ansi.FColor color, ColoredAction coloredAction) {
        console.setForegroundColor(color);
        coloredAction.execute();
        console.setForegroundColor(Ansi.FColor.WHITE);
    }
}
