package org.scrum.psd.battleship.ascii;

import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;

import java.util.NoSuchElementException;

import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;

public class MainEndToEndTest {
    @ClassRule
    public static final SystemOutRule systemOutRule = new SystemOutRule().enableLog();
    @ClassRule
    public static final TextFromStandardInputStream gameInput = emptyStandardInputStream();

    @Test
    public void testPlayGameShotHits() {
        try {
            gameInput.provideLines("a1", "d", "b1", "d", "c1", "d", "d1", "d", "e1", "d", "b4", "b1", "b8", "b6", "d1", "d2");

            Main.main(new String[]{});
        } catch (NoSuchElementException e) {
            Assert.assertTrue(systemOutRule.getLog().contains("Welcome to Battleship"));
            Assert.assertTrue(systemOutRule.getLog().contains("Yeah ! Nice hit !"));
        }
    }

    @Test
    public void testPlayGameShotMisses() {
        try {
            gameInput.provideLines("a1", "d", "b1", "d", "c1", "d", "d1", "d", "e1", "d", "a1", "a8");

            Main.main(new String[]{});
        } catch (NoSuchElementException e) {
            Assert.assertTrue(systemOutRule.getLog().contains("Welcome to Battleship"));
            Assert.assertTrue(systemOutRule.getLog().contains("Miss"));
        }
    }

    @Test
    public void testPlayGameShotWrongValue() {
        try {
            gameInput.provideLines("a1", "d", "b1", "d", "c1", "d", "d1", "d", "e1", "d", "ы1", "b4");

            Main.main(new String[]{});
        } catch (NoSuchElementException e) {
            Assert.assertTrue(systemOutRule.getLog().contains("Welcome to Battleship"));
            Assert.assertTrue(systemOutRule.getLog().contains("Wrong value. Enter coordinates for your shot"));
            Assert.assertTrue(systemOutRule.getLog().contains("Yeah ! Nice hit !"));
        }
    }

    @Test
    public void testPlayGameDestroyShipDoesNotAppearsInSurviveList() {
        try {
            gameInput.provideLines("a1", "d", "b1", "d", "c1", "d", "d1", "d", "e1", "d",
                    "b1",
                    "C1",
                    "D1",
                    "E1",
                    "F1",
                    "B3",
                    "B4",
                    "B5",
                    "B6",
                    "D7",
                    "E7",
                    "F7",
                    "D5",
                    "E5",
                    "F5",
                    "H2",
                    "H3",
                    "B8",
                    "C8",
                    "D8",
                    "E8",
                    "F8",
                    "D2",
                    "E2",
                    "F2",
                    "G2",
                    "E4",
                    "F4",
                    "G4",
                    "A5",
                    "B5",
                    "C5",
                    "H6",
                    "H7",
                    "B6",
                    "C6",
                    "D6",
                    "E6",
                    "F6",
                    "G1",
                    "G2",
                    "G3",
                    "G4",
                    "C2",
                    "D2",
                    "E2",
                    "A2",
                    "A3",
                    "A4",
                    "H7",
                    "H8",
                    "B2",
                    "C2",
                    "D2",
                    "E2",
                    "F2",
                    "F5",
                    "F6",
                    "F7",
                    "F8",
                    "B7",
                    "C7",
                    "D7",
                    "H3",
                    "H4",
                    "H5",
                    "A4",
                    "A5",
                    "H2",
                    "H3",
                    "H4",
                    "H5",
                    "H6",
                    "E5",
                    "E6",
                    "E7",
                    "E8",
                    "B1",
                    "C1",
                    "D1",
                    "C3",
                    "D3",
                    "E3",
                    "A6",
                    "A7");
            Main.main(new String[]{});
        } catch (NoSuchElementException e) {
            Assert.assertTrue(systemOutRule.getLog().contains("Welcome to Battleship"));
            Assert.assertTrue(systemOutRule.getLog().contains("YOU WIN"));
        }
    }
}
